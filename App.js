import React, { Component } from 'react';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { StyleProvider } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';

import Main from './src/Main';
import persist from "./src/config/store";

const persistStore = persist();

export default class App extends Component {
    render() {
        return (
            <StyleProvider style={getTheme(platform)}>
                <Provider store={persistStore.store}>
                    <PersistGate loading={null} persistor={persistStore.persistor}>
                        <Main />
                    </PersistGate>
                </Provider>
            </StyleProvider>
        )
    }
}
