Ejeutar el proyecto:
    1. Ejecutar en un consola de comandos 'npm start'
    2. Ejecutar en otra consola de comandos 'npx react-native run-android' o 'react-native run-android' 

Node: 12.13.0

Errores:

1. Unable to load script. Make sure you're either running a Metro server (run 'react-native start') or that your bundle 'index.android.bundle' is packaged correctly for release.
    - Se debe ejecutar el comando 'npm start' en la raiz del proyecto para inicial el servidor.

2. Error de expresion regular al realizar el comando 'npm start'
    - Este error se presenta en las versiones de Node >= 12.11.0:
    Abrir el archivo  node_modules\metro-config\src\defaults\blacklist.js
      
    Modificar la siguiente expresion regular:
        var sharedBlacklist = [
            /node_modules[/\\]react[/\\]dist[/\\].*/,
            /website\/node_modules\/.*/,
            /heapCapture\/bundle\.js/,
            /.*\/__tests__\/.*/
        ];

    Por la siguiente:
        var sharedBlacklist = [
            /node_modules[\/\\]react[\/\\]dist[\/\\].*/,
            /website\/node_modules\/.*/,
            /heapCapture\/bundle\.js/,
            /.*\/__tests__\/.*/
        ];