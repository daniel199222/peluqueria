import React, { Component } from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';

import Login from '../pages/Login';
import Signup from '../pages/Signup';
import Profile from '../pages/Profile';
import Menu from '../pages/Menu';
import ListEmpleado from '../pages/empleado/listar';
import EmpleadoForm from '../pages/empleado/form';
import ListTipoServicio from '../pages/tipoServicio/listar';
import TipoServicioForm from '../pages/tipoServicio/form';
import ListServicio from '../pages/servicio/listar';
import ServicioForm from '../pages/servicio/form';
import Reporte from '../pages/reporte';

export default class Routes extends Component<{}> {
    render() {
        return (
            <Router>
                <Scene>
                    <Scene key="root" hideNavBar={true} initial={!this.props.isLoggedIn}>
                        <Scene key="login" component={Login} title="Login" initial={true} />
                        <Scene key="register" component={Signup} title="Register" />
                    </Scene>
                    <Scene key="app" hideNavBar={true} initial={this.props.isLoggedIn}>
                        <Scene key="menu" component={Menu} title="Menu"/>
                        <Scene key="profile" component={Profile} title="Profile"/>
                        <Scene key="empleado" component={ListEmpleado} title="Empleado"/>
                        <Scene key="empleadoForm" component={EmpleadoForm} title="Formulario Empleado"/>
                        <Scene key="reportes" component={Reporte} title="Reporte"/>
                        <Scene key="servicio" component={ListServicio} title="Servicio"/>
                        <Scene key="servicioForm" component={ServicioForm} title="Formulario Servicio"/>
                        <Scene key="tiposervicio" component={ListTipoServicio} title="TipoServicio"/>
                        <Scene key="tipoServicioForm" component={TipoServicioForm} title="Formulario Tipo Servicio"/>
                    </Scene>
                </Scene>
            </Router>
        )
    }
}