import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    Animated, 
} from 'react-native';

export default class Logo extends Component<{}> {
    render() {
        const height     = this.props.height ? this.props.height : 140;
        this.imageHeight = new Animated.Value(height);

        return (
            <View style={styles.container}>
                {/* <Image style={{ width: 200, height: 140 }} source={require('../images/Logo.png')} /> */}
                <Animated.Image source={require('../images/Logo.png')} style={{ width: 200, height: this.imageHeight }} />
                <Text style={styles.logoText}>Bienvenido</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    logoText: {
        marginVertical: 15,
        fontSize: 18,
        color: 'rgba(255, 255, 255, 0.7)'
    }
});