import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Field, reduxForm } from 'redux-form';
import { connect } from "react-redux";
import { compose } from "redux";
import { Container, Header, Content, Form, Item } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Logo from '../components/Logo';
import InputText from "../components/InputText";
import { createNewUser } from "../actions/auth.actions";
import Loader from "../components/Loader";
import { ErrorUtils } from "../utils/auth.utils";

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row'
    },
    signupText: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16
    },
    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500',
        paddingLeft: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "500",
        color: "#ffffff",
        textAlign: "center"
    },
    errorText: {
        color: "#ffffff",
        fontSize: 14,
        paddingHorizontal: 16,
        paddingBottom: 8
    }
});

class Signup extends Component<{}> {

    goBack() {
        Actions.login()
    }

    createNewUser = async (values) => {
        try {
            const response = await this.props.dispatch(createNewUser(values));
            if (!response.success) {
                throw response;
            } else {

                Alert.alert(
                    'Registro Exitoso',
                    "Usuario creado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );

                this.goBack();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Registro Error");
            newError.showAlert();
        }
    }

    onSubmit = (values) => {
        this.createNewUser(values);
    }

    renderTextInput = (field) => {
        const { meta: { touched, error }, label, secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput } } = field;
        return (
            <View>
                <InputText
                    onChangeText={onChange}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    label={label}
                    {...restInput} />
                {(touched && error) && <Text style={styles.errorText}>{error}</Text>}
            </View>
        );
    }

    render() {

        const { handleSubmit, createUser } = this.props;

        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    {createUser.isLoading && <Loader />}
                    <Logo />
                    <Form>
                        <Item>
                            <Field
                                name="first_name"
                                placeholder="Nombre"
                                component={this.renderTextInput} />
                        </Item>
                        <Item>
                            <Field
                                name="last_name"
                                placeholder="Apellidos"
                                component={this.renderTextInput} />
                        </Item>
                        <Item>
                            <Field
                                name="email"
                                placeholder="Email"
                                component={this.renderTextInput} />
                        </Item>
                        <Item>
                            <Field
                                name="username"
                                placeholder="Usuario"
                                component={this.renderTextInput} />
                        </Item>
                        <Item>
                            <Field
                                name="password"
                                placeholder="Contraseña"
                                secureTextEntry={true}
                                component={this.renderTextInput} />
                        </Item>
                    </Form>
                    <TouchableOpacity style={styles.button} onPress={handleSubmit(this.onSubmit)}>
                        <Text style={styles.buttonText}>Enviar</Text>
                    </TouchableOpacity>
                    <View style={styles.signupTextCont}>
                        <Text style={styles.signupText}>¿Ya te encuentras registrado?</Text>
                        <TouchableOpacity onPress={this.goBack}><Text style={styles.signupButton}>Login</Text></TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.firstname) {
        errors.firstname = "Este campo es requerido"
    }

    if (!values.lastname) {
        errors.lastname = "Este campo es requerido"
    }

    if (!values.email) {
        errors.email = "Este campo es requerido"
    }

    if (!values.username) {
        errors.username = "Este campo es requerido"
    }

    if (!values.password) {
        errors.password = "Este campo es requerido"
    }
    return errors;
}

mapStateToProps = (state) => ({
    createUser: state.authReducer.createUser
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'register',
        validate
    })
)(Signup)