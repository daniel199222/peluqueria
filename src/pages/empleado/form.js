import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Navigator,
    Alert
} from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from "react-redux";
import { compose } from "redux";
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List, ListItem, Form, Item, Picker } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import InputText from "../../components/InputText";
import Loader from "../../components/Loader";
import { ErrorUtils } from "../../utils/auth.utils";
import { Actions } from 'react-native-router-flux';

import { createNewEmpleado, UpdateEmpleado, DeleteEmpleado } from "../../actions/empleado.actions";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#455a64'
    },
    botones: {
        alignItems: 'center',
        marginVertical: 20,
    },
    botonCrear: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },
    botonList: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 10,
        marginVertical: 8,
    },
    estiloCrear: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },
    botonActualizar: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },

    estiloActualizar: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },

    botonEliminar: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },

    estiloEliminar: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },
})

class EmpleadoForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: undefined,
        };
    }

    goBack() {
        this.props.dispatch({ type: "CRUD_EMPLEADO_CLEAR" });
        Actions.empleado();
    }

    onSubmit = (values) => {
        let registro = values;

        if (this.state.id && this.state.id !== null) {
            registro.id = this.state.id;
            this.update(registro);
        } else {
            this.create(registro);
        }
    }

    create = async (values) => {
        try {
            const response = await this.props.dispatch(createNewEmpleado(values));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Empleado',
                    "Registro creado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );
                this.goBack();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    update = async (values) => {
        try {
            const response = await this.props.dispatch(UpdateEmpleado(values));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Empleado',
                    "Registro actualizado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );
                this.goBack();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    delete = async () => {

        let registro = {
            id: this.state.id
        }

        try {
            const response = await this.props.dispatch(DeleteEmpleado(registro));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Empleado',
                    "Registro eliminado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );
                this.goBack();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    componentDidMount() {
        const { crudEmpleado } = this.props;

        if (crudEmpleado && crudEmpleado.data) {
            this.setState({
                id: crudEmpleado.data.id
            });
        }
    }

    renderTextInput = (field) => {
        const { meta: { touched, error }, label, secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput } } = field;
        return (
            <View>
                <InputText
                    onChangeText={onChange}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    label={label}
                    {...restInput} />
                {(touched && error) && <Text style={styles.errorText}>{error}</Text>}
            </View>
        );
    }

    render() {

        const { handleSubmit, crudEmpleado } = this.props;
        console.log("this.state", this.state);

        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => { this.goBack() }}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Formulario Empleado</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={styles.content}>
                    {(crudEmpleado && crudEmpleado.isLoading) && <Loader />}
                    <KeyboardAwareScrollView>
                        <View style={styles.container}>
                            <Form>
                                <Item>
                                    <Field
                                        name="codigo"
                                        placeholder="Codigo del Empleado"
                                        component={this.renderTextInput} />
                                </Item>
                                <Item>
                                    <Field
                                        name="nombre"
                                        placeholder="Nombre del empleado"
                                        component={this.renderTextInput} />
                                </Item>
                                <Item>
                                    <Field
                                        name="cargo"
                                        placeholder="Cargo del Empleado"
                                        component={this.renderTextInput} />
                                </Item>
                                <Item>
                                    <Field
                                        name="porcentaje"
                                        placeholder="Porcentaje"
                                        component={this.renderTextInput} />
                                </Item>
                            </Form>
                            <View style={styles.botones}>
                                {
                                    (!this.state.id) ? (
                                        <TouchableOpacity style={styles.botonCrear} onPress={handleSubmit(this.onSubmit)}>
                                            <Text style={styles.estiloCrear}>
                                                Crear
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                                {
                                    (this.state.id && this.state.id !== null) ? (
                                        <TouchableOpacity style={styles.botonActualizar} onPress={handleSubmit(this.onSubmit)}>
                                            <Text style={styles.estiloActualizar}>
                                                Actualizar
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                                {
                                    (this.state.id && this.state.id !== null) ? (
                                        <TouchableOpacity style={styles.botonEliminar} onPress={this.delete}>
                                            <Text style={styles.estiloEliminar}>
                                                Eliminar
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </Content>
            </Container>
        )
    }
}

const validate = (values) => {
    const errors = {};

    //El campo codigo es requerido o se genera automaticamente ?    

    if (!values.codigo) {
        errors.codigo = "Este campo es requerido"
    }
    if (!values.nombre) {
        errors.nombre = "Este campo es requerido"
    }
    if (!values.cargo) {
        errors.cargo = "Este campo es requerido"
    }
    if (!values.porcentaje) {
        errors.porcentaje = "Este campo es requerido"
    }

    return errors;
}

mapStateToProps = (state) => ({
    crudEmpleado: state.empleadoReducer.crudEmpleado,
    initialValues: state.empleadoReducer.crudEmpleado.data,
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'empleado',
        validate,
        enableReinitialize: true
    })
)(EmpleadoForm)
