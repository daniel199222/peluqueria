import React, { Component } from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { ListItem } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "redux";
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List } from 'native-base';

import { FindAllEmpleado } from '../../actions/empleado.actions';
import Loader from "../../components/Loader";

import { Actions } from 'react-native-router-flux';
import { logoutUser } from "../../actions/auth.actions";

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
  }
});

class ListEmpleado extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {

    this.props.dispatch(FindAllEmpleado())
      .then(res => {
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });

  }

  envioID(idEmp) {
    Actions.servicio({
      passProps: { idEmp }
    });
  }

  goBack = () => {
    Actions.menu();
  }

  perfil = () => {
    Actions.profile();
  }

  logoutUser = () => {
    this.props.dispatch(logoutUser());
  }

  add = () => {
    Actions.empleadoForm();
  }

  seleccionar = (item) => {    
    this.props.dispatch({ type: "CRUD_EMPLEADO_GET", payload: item});
    this.add();
  }

  render() {
    const { empleadoDetails } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => { this.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Empleados</Title>
          </Body>
          <Right>
          <Button transparent onPress={() => { this.add() }}>
              <Icon name='plus' type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => { this.perfil() }}>
              <Icon name='account' type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => { this.logoutUser() }}>
              <Icon name='logout' type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>
        <Content style={styles.content}>
          {(empleadoDetails && empleadoDetails.isLoading) && <Loader />}
          <FlatList
            keyExtractor={(item) => item.id}
            data={empleadoDetails.empleadoDetails}
            renderItem={({ item }) => (

              <ListItem
                subtitle={item.cargo}
                title={item.nombre}
                bottomDivider
                chevron
                button 
                onPress={() => { this.seleccionar(item) }}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

mapStateToProps = (state) => ({
  empleadoDetails: state.empleadoReducer.getEmpleado
});

mapDispatchToProps = (dispatch) => ({
  dispatch
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(ListEmpleado);