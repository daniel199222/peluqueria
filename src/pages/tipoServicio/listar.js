import React, { Component } from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { ListItem } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "redux";
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List } from 'native-base';

import { FindAllTipoServicio } from '../../actions/tiposervicio.actions';
import Loader from "../../components/Loader";

import { Actions } from 'react-native-router-flux';
import { logoutUser } from "../../actions/auth.actions";

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
  }
});

class ListTipoServicio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {

    this.props.dispatch(FindAllTipoServicio())
      .then(res => {
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });

  }

  envioID(idEmp) {
    Actions.tiposervicio({
      passProps: { idEmp }
    });
  }

  goBack = () => {
    Actions.menu();
  }

  perfil = () => {
    Actions.profile();
  }

  logoutUser = () => {
    this.props.dispatch(logoutUser());
  }

  add = () => {
    Actions.tipoServicioForm();
  }

  seleccionar = (item) => {    
    this.props.dispatch({ type: "CRUD_TIPOSERVICIO_GET", payload: item});
    this.add();
  }

  render() {
    const { tiposervicioDetails } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => { this.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Tipos de Servicios</Title>
          </Body>
          <Right>
          <Button transparent onPress={() => { this.add() }}>
              <Icon name='plus' type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => { this.perfil() }}>
              <Icon name='account' type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => { this.logoutUser() }}>
              <Icon name='logout' type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>
        <Content style={styles.content}>
          {(tiposervicioDetails && tiposervicioDetails.isLoading) && <Loader />}
          <FlatList
            keyExtractor={(item) => item.id}
            data={tiposervicioDetails.tiposervicioDetails}
            renderItem={({ item }) => (

              <ListItem
                subtitle={item.id}
                title={item.nombre}
                bottomDivider
                chevron
                button 
                onPress={() => { this.seleccionar(item) }}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

mapStateToProps = (state) => ({
  tiposervicioDetails: state.tiposervicioReducer.getTipoServicio
});

mapDispatchToProps = (dispatch) => ({
  dispatch
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(ListTipoServicio);