import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Alert
} from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from "react-redux";
import { compose } from "redux";
import { Actions } from 'react-native-router-flux';

import Logo from '../components/Logo';
import InputText from "../components/InputText";
import Loader from "../components/Loader";
import { loginUser } from "../actions/auth.actions";
import {ErrorUtils} from "../utils/auth.utils";

class Login extends Component<{}> {

    signup() {
        Actions.register();
    }

    loginUser = async (values) => {
        try {
            const response = await this.props.dispatch(loginUser(values));
            if (!response.success) {
                throw response;
            } else {
                Actions.menu();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Login Error");
            newError.showAlert();
        }
    }

    onSubmit = (values) => {
        this.loginUser(values);
    }

    renderTextInput = (field) => {
        const { meta: { touched, error }, label, secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput } } = field;
        return (
            <View>
                <InputText
                    onChangeText={onChange}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    label={label}
                    {...restInput} />
                {(touched && error) && <Text style={styles.errorText}>{error}</Text>}
            </View>
        );
    }

    render() {

        const { handleSubmit, loginUser } = this.props;

        return (
            <View style={styles.container}>
                {(loginUser && loginUser.isLoading) && <Loader />}
                <Logo />
                <Field
                    name="username"
                    placeholder="Usuario"
                    component={this.renderTextInput} />
                <Field
                    name="password"
                    placeholder="Contraseña"
                    secureTextEntry={true}
                    component={this.renderTextInput} />
                <TouchableOpacity style={styles.button} onPress={handleSubmit(this.onSubmit)}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>¿No tienes una cuenta aun?</Text>
                    <TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}>Registrate</Text></TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row'
    },
    signupText: {
        color: 'rgba(255, 255, 255, 0.7)',
        fontSize: 16
    },
    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500',
        paddingLeft: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "500",
        color: "#ffffff",
        textAlign: "center"
    },
});

const validate = (values) => {
    const errors = {};

    if (!values.username) {
        errors.username = "Este campo es requerido"
    }

    if (!values.password) {
        errors.password = "Este campo es requerido"
    }
    return errors;
}

mapStateToProps = (state) => ({
    loginUser: state.authReducer.loginUser
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'login',
        validate
    })
)(Login)