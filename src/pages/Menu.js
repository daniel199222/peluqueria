import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List, ListItem } from 'native-base';
import { connect } from "react-redux";

import Logo from '../components/Logo';
import { logoutUser } from "../actions/auth.actions";

class Menu extends React.Component {
  
  empleado = () => {
    Actions.empleado();
  }

  servicio = () => {
    Actions.servicio();
  }

  tipoServicio = () => {
    Actions.tiposervicio();
  }

  reportes = () => {
    Actions.reportes();
  }

  perfil = () => {
    Actions.profile();
  }

  logoutUser = () => {
    this.props.dispatch(logoutUser());
  }

  render() {

    return (
      <Container style={styles.container}>
        <Header>
          <Left />
          <Body>
            <Title>Home</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => { this.perfil() }}>
              <Icon name='account' type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => { this.logoutUser() }}>
              <Icon name='logout' type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>
        <Content style={styles.content}>
          <Logo />
          <List style={{ backgroundColor: "#fff" }}>
            <ListItem icon button onPress={() => { this.empleado() }}>
              <Body>
                <Text>Empleados</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon button onPress={() => { this.tipoServicio() }}>
              <Body>
                <Text>Tipos de Servicios</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon button onPress={() => { this.servicio() }}>
              <Body>
                <Text>Servicios</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <ListItem icon button onPress={() => { this.reportes() }}>
              <Body>
                <Text>Reporte</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
  },
  content: {
    paddingTop: 20,
  },
  button: {
    marginTop: 10,
  },
});

mapDispatchToProps = (dispatch) => ({
  dispatch
});

export default connect(null, mapDispatchToProps)(Menu)