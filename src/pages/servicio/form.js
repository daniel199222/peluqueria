import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Navigator,
    Alert
} from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from "react-redux";
import { compose } from "redux";
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List, ListItem, Form, Item, Picker } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import InputText from "../../components/InputText";
import Loader from "../../components/Loader";
import { ErrorUtils } from "../../utils/auth.utils";
import { Actions } from 'react-native-router-flux';

import { createNewServicio, UpdateServicio, DeleteServicio } from "../../actions/servicio.actions";
import { FindAllEmpleado } from "../../actions/empleado.actions";
import { FindAllTipoServicio } from "../../actions/tiposervicio.actions";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#455a64'
    },
    botones: {
        alignItems: 'center',
        marginVertical: 20,
    },
    botonCrear: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },
    botonList: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 10,
        marginVertical: 8,
    },
    estiloCrear: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },
    botonActualizar: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },

    estiloActualizar: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },

    botonEliminar: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        paddingVertical: 6,
        marginVertical: 8,
    },

    estiloEliminar: {
        color: '#ffffff',
        fontSize: 15,
        fontWeight: '500',
        textAlign: 'center',
    },
})
class ServicioForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            empleados: [],
            tiposServicios: [],
            id: undefined,
            id_empleado: undefined,
            id_tiposervicio: undefined
        };
    }

    goBack() {
        this.props.dispatch({ type: "CRUD_SERVICIO_CLEAR"});
        Actions.servicio();
    }

    onSubmit = (values) => {
        let servicio = values;
        servicio.id_empleado = this.state.id_empleado;
        servicio.id_tiposervicio = this.state.id_tiposervicio;
        
        if (this.state.id && this.state.id !== null ) {
            servicio.id = this.state.id;
            this.update(servicio);
        } else {
            this.create(servicio);
        }
    }

    create = async (values) => {
        try {
            const response = await this.props.dispatch(createNewServicio(values));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Servicio',
                    "Registro creado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );

                this.goBack();
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    update = async (values) => {
        try {
            const response = await this.props.dispatch(UpdateServicio(values));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Servicio',
                    "Registro actualizado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );

                this.goBack()
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    delete = async () => {

        let registro = {
            id: this.state.id
        }

        try {
            const response = await this.props.dispatch(DeleteServicio(registro));
            if (!response.success) {
                throw response;
            } else {
                Alert.alert(
                    'Servicio',
                    "Registro eliminado exitosamente",
                    [
                        {
                            text: 'Cerrar',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                        },
                    ],
                );

                this.goBack()
            }
        } catch (error) {
            const newError = new ErrorUtils(error, "Servicio Error");
            newError.showAlert();
        }
    }

    componentDidMount() {
        this.makeRemoteRequest();

        const { crudServicio } = this.props;

        if (crudServicio && crudServicio.data) {
            this.setState({
                id: crudServicio.data.id
            });
            this.setState({
                id_empleado: crudServicio.data.id_empleado
            });
            this.setState({
                id_tiposervicio: crudServicio.data.id_tiposervicio
            });
        }
    }

    makeRemoteRequest = () => {

        this.props.dispatch(FindAllEmpleado())
            .then(res => {
                this.setState({
                    empleados: res.responseBody.data
                });
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });

        this.props.dispatch(FindAllTipoServicio())
            .then(res => {
                this.setState({
                    tiposServicios: res.responseBody.data
                });
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });

    }

    irTipoServicio = () => {
        Actions.tiposervicio();
    }

    irListEmpleado = () => {
        Actions.listempleado();
    }

    renderTextInput = (field) => {
        const { meta: { touched, error }, label, secureTextEntry, maxLength, keyboardType, placeholder, input: { onChange, ...restInput } } = field;
        return (
            <View>
                <InputText
                    onChangeText={onChange}
                    maxLength={maxLength}
                    placeholder={placeholder}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    label={label}
                    {...restInput} />
                {(touched && error) && <Text style={styles.errorText}>{error}</Text>}
            </View>
        );
    }

    onValueChangeEmpleado(value: string) {
        this.setState({
            id_empleado: value
        });
    }

    onValueChangeTipoServicio(value: string) {
        this.setState({
            id_tiposervicio: value
        });
    }

    render() {

        const { handleSubmit, crudServicio } = this.props;
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => { this.goBack() }}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Formulario Servicios</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={styles.content}>
                    {(crudServicio && crudServicio.isLoading) && <Loader />}
                    <KeyboardAwareScrollView>
                        <View style={styles.container}>
                            <Form>
                                <Item picker>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        placeholder="Empleado"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.id_empleado}
                                        onValueChange={this.onValueChangeEmpleado.bind(this)}
                                    >
                                        {
                                            this.state.empleados.map((item) => {
                                                return (
                                                    <Picker.Item label={item.nombre} value={item.id} key={item.id} />
                                                );
                                            })
                                        }
                                    </Picker>
                                </Item>
                                <Item picker>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={<Icon name="arrow-down" />}
                                        style={{ width: undefined }}
                                        placeholder="Tipo de Servicio"
                                        placeholderStyle={{ color: "#bfc6ea" }}
                                        placeholderIconColor="#007aff"
                                        selectedValue={this.state.id_tiposervicio}
                                        onValueChange={this.onValueChangeTipoServicio.bind(this)}
                                    >
                                        {
                                            this.state.tiposServicios.map((item) => {
                                                return (
                                                    <Picker.Item label={item.nombre} value={item.id} key={item.id} />
                                                );
                                            })
                                        }
                                    </Picker>
                                </Item>
                                <Item>
                                    <Field
                                        name="valor"
                                        placeholder="Valor del Servicio"
                                        component={this.renderTextInput} />
                                </Item>
                                <Item>
                                    <Field
                                        name="fecha"
                                        placeholder="Fecha del Servicio"
                                        component={this.renderTextInput} />
                                </Item>
                            </Form>
                            <View style={styles.botones}>
                                {
                                    (!this.state.id) ? (
                                        <TouchableOpacity style={styles.botonCrear} onPress={handleSubmit(this.onSubmit)}>
                                            <Text style={styles.estiloCrear}>
                                                Crear
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                                {
                                    (this.state.id && this.state.id !== null) ? (
                                        <TouchableOpacity style={styles.botonActualizar} onPress={handleSubmit(this.onSubmit)}>
                                            <Text style={styles.estiloActualizar}>
                                                Actualizar
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                                {
                                    (this.state.id && this.state.id !== null) ? (
                                        <TouchableOpacity style={styles.botonEliminar} onPress={ this.delete }>
                                            <Text style={styles.estiloEliminar}>
                                                Eliminar
                                            </Text>
                                        </TouchableOpacity>
                                    ) : null
                                }
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </Content>
            </Container>
        )
    }
}

const validate = (values) => {
    const errors = {};
    {/*
    if (!values.id_empleado) {
        errors.id_empleado = "Este campo es requerido"
    }

    if (!values.tiposervicio) {
        errors.tiposervicio = "Este campo es requerido"
    }*/}
    if (!values.valor) {
        errors.valor = "Este campo es requerido"
    }
    if (!values.fecha) {
        errors.fecha = "Este campo es requerido"
    }
    return errors;
}

mapStateToProps = (state) => ({
    crudServicio: state.servicioReducer.crudServicio,
    initialValues: state.servicioReducer.crudServicio.data,
    getEmpleado: state.servicioReducer.getEmpleado
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: 'service',
        validate,
        enableReinitialize: true
    })
)(ServicioForm)