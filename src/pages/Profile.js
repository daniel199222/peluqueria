import React, { Component } from 'react';
import Menu from './Menu'
import Logo from '../components/Logo'
import {
    StyleSheet,
    View,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import { connect } from "react-redux";
import { Container, Header, Left, Right, Body, Title, Content, Button, Icon, Text, List, ListItem } from 'native-base';

import { logoutUser } from "../actions/auth.actions";
import { Actions } from 'react-native-router-flux';
import { Provider } from 'react-native-paper';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
    },
    textStyle: {
        color: "#fff",
        fontSize: 18
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "500",
        color: "#ffffff",
        textAlign: "center"
    },
});

class Profile extends Component<{}> {

    goBack = () => {
        Actions.pop();
    }

    logoutUser = () => {
        this.props.dispatch(logoutUser());
    }

    render() {
        const { getUser: { userDetails } } = this.props;
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => { this.goBack() }}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Perfil</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => { this.logoutUser() }}>
                            <Icon name='logout' type="MaterialCommunityIcons" />
                        </Button>
                    </Right>
                </Header>
                <Content style={styles.content}>
                    <Text style={styles.textStyle}>Nombre (s): {userDetails ? userDetails.first_name : ""}</Text>
                    <Text style={styles.textStyle}>Apellidos (s): {userDetails ? userDetails.last_name : ""}</Text>
                    <Text style={styles.textStyle}>Usuario: {userDetails ? userDetails.username : ""}</Text>
                    <Text style={styles.textStyle}>Email: {userDetails ? userDetails.email : ""}</Text>
                </Content>
            </Container>
        )
    }
}

mapStateToProps = (state) => ({
    getUser: state.userReducer.getUser
});

mapDispatchToProps = (dispatch) => ({
    dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile)