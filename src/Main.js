import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import {connect} from "react-redux";

import Routes from './components/Routes';

class Main extends Component<{}> {

    state = {
        isLoggedIn: this.props.authData ? this.props.authData.isLoggedIn : false
    }

    static getDerivedStateFormProps(nextProps, nextState) {
        if (nextProps.authData && nextProps.authDat.isLoggedIn && nextProps.authDat.isLoggedIn !== nextState.isLoggedIn) {
            return {
                isLoggedIn: nextProps.authDat.isLoggedIn
            }
        }
        return null;
    }

    shouldComponentUpdate(nextProps, nextState) {
        const {authData: {isLoggedIn}} = nextProps;
        if (isLoggedIn !== nextState.isLoggedIn) {
            return true;
        }
        return true;
    }

    render() {
        const {authData} = this.props;
        let isLoggedIn = authData.isLoggedIn ? authData.isLoggedIn : false;
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#1c313a" barStyle="light-content" />
                <Routes isLoggedIn={isLoggedIn} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

mapStateToProps = state => ({
    authData: state.authReducer.authData
})

export default connect(mapStateToProps, null)(Main);