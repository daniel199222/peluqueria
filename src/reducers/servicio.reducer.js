import { combineReducers } from "redux";

const getServicio = (state = {}, action) => {
    switch (action.type) {
        case "GET_SERVICIO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                servicioDetails: null,
                errors: null
            };
        case "GET_SERVICIO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                servicioDetails: action.payload,
                errors: null
            };
        case "GET_SERVICIO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                servicioDetails: null,
                errors: action.payload
            };

        default:
            return state;
    }
}

const crudServicio = (state = {}, action) => {
    switch (action.type) {
        case "CRUD_SERVICIO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                data: null,
                errors: null
            };
        case "CRUD_SERVICIO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_SERVICIO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                data: null,
                errors: action.payload
            };
        case "CRUD_SERVICIO_GET":
            action.payload.valor = action.payload.valor.toString();
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_SERVICIO_CLEAR":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: null,
                errors: null
            };

        default:
            return state;
    }
}

export default combineReducers({
    getServicio,
    crudServicio
});