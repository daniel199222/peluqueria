import { combineReducers } from "redux";

const getTipoServicio = (state = {}, action) => {
    switch (action.type) {
        case "GET_TIPOSERVICIO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                tiposervicioDetails: null,
                errors: null
            };
        case "GET_TIPOSERVICIO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                tiposervicioDetails: action.payload,
                errors: null
            };
        case "GET_TIPOSERVICIO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                tiposervicioDetails: null,
                errors: action.payload
            };

        default:
            return state;
    }
}

const crudTipoServicio = (state = {}, action) => {
    switch (action.type) {
        case "CRUD_TIPOSERVICIO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                data: null,
                errors: null
            };
        case "CRUD_TIPOSERVICIO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_TIPOSERVICIO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                data: null,
                errors: action.payload
            };
        case "CRUD_TIPOSERVICIO_GET":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_TIPOSERVICIO_CLEAR":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: null,
                errors: null
            };

        default:
            return state;
    }
}

export default combineReducers({
    crudTipoServicio,
    getTipoServicio
});