import { combineReducers } from "redux";

const getEmpleado = (state = {}, action) => {
    switch (action.type) {
        case "GET_EMPLEADO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                empleadoDetails: null,
                errors: null
            };
        case "GET_EMPLEADO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                empleadoDetails: action.payload,
                errors: null
            };
        case "GET_EMPLEADO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                empleadoDetails: null,
                errors: action.payload
            };

        default:
            return state;
    }
}

const crudEmpleado = (state = {}, action) => {
    switch (action.type) {
        case "CRUD_EMPLEADO_LOADING":
            return {
                isLoading: true,
                isError: false,
                isSucces: false,
                data: null,
                errors: null
            };
        case "CRUD_EMPLEADO_SUCCESS":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_EMPLEADO_FAIL":
            return {
                isLoading: false,
                isError: true,
                isSucces: false,
                data: null,
                errors: action.payload
            };
        case "CRUD_EMPLEADO_GET":
            action.payload.porcentaje = action.payload.porcentaje.toString();
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: action.payload,
                errors: null
            };
        case "CRUD_EMPLEADO_CLEAR":
            return {
                isLoading: false,
                isError: false,
                isSucces: true,
                data: null,
                errors: null
            };

        default:
            return state;
    }
}

export default combineReducers({
    getEmpleado,
    crudEmpleado,
});