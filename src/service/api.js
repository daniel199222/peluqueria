const BASE_URL = "http://ec2-18-222-155-133.us-east-2.compute.amazonaws.com:3000";
// const BASE_URL = "http://192.168.56.1:3000";

export const api = async (url, method, body = null, headers = {}) => {

    try {
        const endPoint = BASE_URL.concat(url);
        const requestBody = body ? JSON.stringify(body) : null;
        const fetchParams = { method, headers };

        if ((method === "POST" && method === "PUT") && !requestBody) {
            throw new Error("Request body requerido");
        }

        if (requestBody) {
            fetchParams.headers["Content-Type"] = "application/json";
            fetchParams.body = requestBody;
        }

        const fetchPromise = fetch(endPoint, fetchParams);
        const timeOutPromise = new Promise((resolve, reject) => {
            setTimeout(() => {
                reject("Request timeout");
            }, 10000);
        });

        const response = await Promise.race([fetchPromise, timeOutPromise]);

        return response;
    } catch (e) {
        throw new Error(e);
    }
}

export const fetchApi = async (url, method, body, statusCode, token = null, loader = false, promiseReturn = "json") => {

    try {

        const headers = {};
        const result = {
            token: null,
            success: false,
            responseBody: null
        };

        if (token) {
            headers["Authorization"] = "JWT ".concat(token);
        }

        const response = await api(url, method, body, headers);

        if (response.status === statusCode) {
            result.success = true;

            if (response.headers.get("Authorization")) {
                result.token = response.headers.get("Authorization");
            }

            let responseBody;
            const responseText = await response.text();
            try {
                responseBody = JSON.parse(responseText);
            } catch (error) {
                responseBody = responseText;
            }

            result.responseBody = responseBody;
            return result;
        }

        let errorBody;
        const errorText = await response.text();
        try {
            errorBody = JSON.parse(errorText);
        } catch (error) {
            errorBody = errorText;
        }

        result.responseBody = errorBody;

        throw result;
    } catch (e) {
        throw e;
    }
}