import { fetchApi } from "../service/api";

export const createNewTipoServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_TIPOSERVICIO_LOADING" });
            const response = await fetchApi("/registerTipoServicio", "POST", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_TIPOSERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_TIPOSERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const UpdateTipoServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_TIPOSERVICIO_LOADING" });
            const response = await fetchApi("/updateTipoServicio", "PUT", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_TIPOSERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_TIPOSERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const DeleteTipoServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_TIPOSERVICIO_LOADING" });
            const response = await fetchApi("/deleteTipoServicio?id=".concat(payload.id), "DELETE", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_TIPOSERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_TIPOSERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindAllTipoServicio = () => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_TIPOSERVICIO_LOADING" });
            const response = await fetchApi("/findAllTipoServicio", "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_TIPOSERVICIO_SUCCESS", payload: response.responseBody.data});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_TIPOSERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindTipoServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_TIPOSERVICIO_LOADING" });
            const response = await fetchApi("/findTipoServicio?id=".concat(payload.id), "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_TIPOSERVICIO_SUCCESS", payload: response.responseBody.data});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_TIPOSERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}