import { fetchApi } from "../service/api";

export const createNewEmpleado = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_EMPLEADO_LOADING" });
            const response = await fetchApi("/registerEmpleado", "POST", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_EMPLEADO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_EMPLEADO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const UpdateEmpleado = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_EMPLEADO_LOADING" });
            const response = await fetchApi("/updateEmpleado", "PUT", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_EMPLEADO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_EMPLEADO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const DeleteEmpleado = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_EMPLEADO_LOADING" });
            const response = await fetchApi("/deleteEmpleado?id=".concat(payload.id), "DELETE", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_EMPLEADO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_EMPLEADO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindAllEmpleado = () => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_EMPLEADO_LOADING" });
            const response = await fetchApi("/findAllEmpleado", "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_EMPLEADO_SUCCESS", payload: response.responseBody.data});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_EMPLEADO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindEmpleado = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_EMPLEADO_LOADING" });
            const response = await fetchApi("/findEmpleado?id=".concat(payload.id), "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_EMPLEADO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_EMPLEADO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}