import { fetchApi } from "../service/api";

export const createNewServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_SERVICIO_LOADING" });
            const response = await fetchApi("/registerServicio", "POST", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_SERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_SERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const UpdateServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_SERVICIO_LOADING" });
            const response = await fetchApi("/updateServicio", "PUT", payload, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_SERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_SERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const DeleteServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "CRUD_SERVICIO_LOADING" });
            const response = await fetchApi("/deleteServicio?id=".concat(payload.id), "DELETE", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "CRUD_SERVICIO_SUCCESS"});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "CRUD_SERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindAllServicio = () => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_SERVICIO_LOADING" });
            const response = await fetchApi("/findAllServicio", "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_SERVICIO_SUCCESS", payload: response.responseBody.data});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_SERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}

export const FindServicio = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        try {
            const {authReducer: {authData: token}} = state;
            dispatch({ type: "GET_SERVICIO_LOADING" });
            const response = await fetchApi("/findServicio?id=".concat(payload.id), "GET", null, 200, token.token);

            if (response.success) {
                dispatch({ type: "GET_SERVICIO_SUCCESS", payload: response.responseBody.data});
            } else {
                throw response;
            }

            return response;

        } catch (error) {
            dispatch({ type: "GET_SERVICIO_FAIL", payload: error.responseBody });
            return error;
        }
    }
}