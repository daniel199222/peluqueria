import React, {Component} from 'react'

import{
    View,
    Text,
    StyleSheet,
    Image,
    Button, 
    StatusBar, 
    TextInput, 
    TouchableOpacity} 
    from 'react-native'
//import login from '.ruta'

class servicios extends Component {
    render() {
        return (
            <View style={style.container}>

                <StatusBar 
                backgroundColor='#1c313a'
                barStyle = 'light-content'
                />

                <View style={style.header}>                

                  <View style={style.headerLeft}>
                      <Image source={require('./assets/imageServicios.png')} 
                             style={style.logo}/>
                  </View>

                  <View style={style.headerRight}>
                     <Text style={style.titulo}> 
                       SERVICOS 
                     </Text>
                  </View>

                </View>

                <View style={style.body}>
                    <View style={style.texto}>
                        <View style={style.textoLeft}>
                            <Text style={style.textobody}>
                                Empleado
                            </Text>
                            <Text style={style.textobody}>
                                Servicio
                            </Text>
                            <Text style={style.textobody}>
                                Valor
                            </Text>
                            <Text style={style.textobody}>
                                Fecha
                            </Text>
                        </View>
                        <View style= {style.textoRight}>
                            <TextInput style={style.inputbody} 
                                       underlineColorAndroid='rgba(0,0,0,0)' 
                                       placeholder='Nombre del Empleado'/>
                            <TextInput style={style.inputbody} 
                                       underlineColorAndroid='rgba(0,0,0,0)' 
                                       placeholder='Servicio Prestado'/>
                            <TextInput style={style.inputbody} 
                                       underlineColorAndroid='rgba(0,0,0,0)' 
                                       placeholder='Valor del Servicio'/>
                            <TextInput style={style.inputbody} 
                                       underlineColorAndroid='rgba(0,0,0,0)' 
                                       placeholder='Fecha del Servicio'/>
                        </View>
                    </View>

                    <View style={style.botones}>
                        <TouchableOpacity style={style.botonCrear}>                  
                            <Text style={style.estiloCrear}>
                                Crear
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={style.botonActualizar}>                  
                            <Text style={style.estiloActualizar}>
                                Actualizar
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={style.botonEliminar}>                  
                            <Text style={style.estiloEliminar}>
                                Eliminar
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
         )
     }
}

const style = StyleSheet.create({
    container : {
        flex : 1,
        flexDirection : 'column',
        backgroundColor : '#455a64'
    },

    header : {
        flex : 0.3,
        flexDirection : 'row',
        marginTop : 25,
        marginEnd : 25,
    },

    headerLeft :{
        flex : 1,
        alignItems : 'center',
    },

    logo : {
        width : 200,
        height :200,
        resizeMode : 'contain',
    },

    headerRight : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    titulo : {
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily : 'Berlin Sans FB',
        textAlign : 'center',
        color : 'white',
    },

    body : {
        flex : 1,
    },

    texto : {
        flex : 2,
        flexDirection : 'row', 
        marginTop : 20,       
    },

    textoLeft : {
        flex : 0.5,
        marginTop : 25,
    },

    textobody : {
        fontFamily : 'Berlin Sans FB',
        fontSize : 20,
        marginLeft : 20,
        marginTop : 30,
        color : 'white',
    },

    textoRight : {
        flex : 1,
        marginTop : 35,

    },

    inputbody : {
        width : 250,
        backgroundColor : 'rgba(255,255,255,0.3)',
        borderRadius : 25,
        marginTop : 10,
    },

    botones : {
        alignItems : 'center',
        marginVertical : 20,
    },

    botonCrear : {
        width : 300,
        backgroundColor : '#1c313a',
        borderRadius : 25,
        paddingVertical : 6,
        marginVertical : 8,
    },

    estiloCrear : {
        color : '#ffffff',
        fontSize : 15,
        fontWeight : '500',
        textAlign : 'center',
    },

    botonActualizar : {
        width : 300,
        backgroundColor : '#1c313a',
        borderRadius : 25,
        paddingVertical : 6,
        marginVertical : 8,
    },

    estiloActualizar : {
        color : '#ffffff',
        fontSize : 15,
        fontWeight : '500',
        textAlign : 'center',
    },

    botonEliminar : {
        width : 300,
        backgroundColor : '#1c313a',
        borderRadius : 25,
        paddingVertical : 6,
        marginVertical : 8,
    },

    estiloEliminar : {
        color : '#ffffff',
        fontSize : 15,
        fontWeight : '500',
        textAlign : 'center',
    },
})

export default servicios